desc: EDO-strobeTuner - a strobe tuner for microtonal guitars (TallKite Software)

/*****************************************************************************
One-time set-up:
Download Reaper or ReaJS, the latter lets you run EDOtuner inside your own DAW (windows only)
If using Reaper:
In Reaper, choose Options/Show Reaper Resource Path, and move this file to the Effects/Meters subfolder
Quit Reaper and re-launch it, so it re-scans and can find this file
Make a new track (control-T) and MUTE IT, to avoid feedback (square "M" button)
Set the track's input to your laptop's microphone (either mono or stereo, it doesn't matter)
Put this effect in the track's FX rack (click the square FX button, type "EDOtuner" in the filter box, then double-click it)
Record-arm the track (round button in the upper left) and turn monitoring on (square button)
Optional: select the track and save it as a track template, for use in other Reaper projects

How to use:
Set the sliders -- default is a standard 12edo 6-string guitar tuned EADGBE
Get the guitar fairly close by ear or by conventional tuner, this tuner is for fine-tuning only
Tune each string to its column, upward drift = too sharp, downward drift = too flat
To enter a number outside a slider's range, type in the little box on the right
To tune a 22edo bass guitar, type in either -1200 cents or -22 frets
If you have multiple microtonal guitars, you can make a preset for each one
You can set the color slider to different amounts so that each preset looks different
To save/rename/delete a preset, click the "+" button in the upper right of the EDOtuner window

To check the placement of cable-tie frets, first tune the open strings really well,
then offset the strobe tuner by 1 fret, then play each string fretted at the 1st fret,
and finally nudge the 1st fret back and forth to correct for sharp/flat notes
Do the same for the other frets, periodically checking that the open strings are still in tune
If one string is always sharp or flat relative to the other strings, adjust the saddle compensation

To use with the Kite guitar, set the edo to 41 and be aware that 1 fret is really 2 edosteps,
or else set the edo to 20.5 and set the string gaps to 6.5 not 13.
You can set a slider to a fractional number by typing in the little box on the far right,
or you can edit the code below and change "slider1: 12 <5, 41, 1>" to "slider1: 12 <5, 41, 0.5>",
and change the "1>" to "0.5>" in slider7 and sliders 11-17 as well
When tuning the 1st string, you can use the 6th string display as well, because 5 strings = 3/1.

If you use this tuner, please consider donating to me via paypal at TallKite.com
Every little bit helps support me in creating useful tools like this one!
**********************************************************************************/

slider1: 12 <5, 41, 1> EDO (equal division of an octave)
slider2: 6 <4, 8, 1> number of guitar strings
slider5: 4 <0, 11, 1 {G#/Ab, G, F#/Gb, F, E, D#/Eb, D, C#/Db, C, B, A#/Bb, A}> pitch of the lowest (bassiest) string
slider6: 0 <-50, 50, 1> cents offset from 12-edo tuned to A-440
slider7: 0 <-1, 50, 1> additional offset in edosteps
slider11: 5 <1, 17, 1> edosteps from 2nd highest string up to the next
slider12: 4 <1, 17, 1> edosteps from 3rd highest string up to the next
slider13: 5 <1, 17, 1> edosteps from 4th highest string up to the next
slider14: 5 <1, 17, 1> edosteps from 5th highest string up to the next
slider15: 5 <1, 17, 1> edosteps from 6th highest string up to the next
slider16: 0 <1, 17, 1> edosteps from 7th highest string up to the next
slider17: 0 <1, 17, 1> edosteps from 8th highest string up to the next
slider20: 0 <-10, 20, 1> brightness
slider21: 0 <0, 10, 1> color

@init
bufpos = 0;				// buffer position
gfx_mode = 1;
Frequency = 0;				// array of 8 frequencies in cycles per sample
Phase = 8;				// array of 8 phases
memset (Phase, 0, 8);
Colors = 16;				// colors of the strobe tuner columns
Colors [0] = 1;		Colors [1] = 0;		Colors [2] = 0; 	// red
Colors [3] = 1;		Colors [4] = 0.5;	Colors [5] = 0; 	// orange
Colors [6] = 1;		Colors [7] = 1;		Colors [8] = 0;		// yellow
Colors [9] = 0;		Colors [10] = 1;	Colors [11] = 0; 	// green
Colors [12] = 0;	Colors [13] = 0.6;	Colors [14] = 1; 	// blue-green
Colors [15] = 0;	Colors [16] = 0;	Colors [17] = 1; 	// blue
Colors [18] = 0.5;	Colors [19] = 0;	Colors [20] = 1; 	// purple
Colors [21] = 1;	Colors [22] = 0;	Colors [23] = 0.7; 	// lavender
Wave = 40;				// endless array of spl values

@slider
edo = slider1; numStrings = slider2;
k = 100 * (11 - slider5) + slider6 + slider7 * 1200 / edo;	// k = cents from A-55
Frequency [0] = 55 * 2 ^ (k / 1200) / srate;			// cycles per sample
i = 1; loop (numStrings - 1,					// i = string number, 0 = lowest
  k = 2 ^ (slider (numStrings + 10 - i) / edo);			// k = ratio between strings as a decimal
  Frequency [i] = k * Frequency [i - 1];
  i += 1;
);
brightness = 2 ^ (slider20 / 5);
colorfulness = 1 - slider21 / 10;

@sample
Wave [bufpos] = abs (spl0);
bufpos += 1;

@gfx 900 400

function drawOrdinal (n) (			// write numbers in an ordinal format
    n == 1 ? gfx_drawstr ("1st") 
  : n == 2 ? gfx_drawstr ("2nd")
  : n == 3 ? gfx_drawstr ("3rd")
  : (gfx_drawNumber (n, 0); gfx_drawstr ("th"));
);

colwid = gfx_w / numStrings;			// column width
gfx_a = 1; gfx_r = gfx_g = gfx_b = 1;		// color = white
gfx_x = 0.5 * colwid - 12; gfx_y = 4;		// 12 is half of the 24 pixels that "Nth" takes up
p = numStrings; loop (numStrings,		// draw the "Nth" label above each column
  drawOrdinal (p);
  gfx_x += colwid - 24;				// "Nth" takes up 24 pixels
  p -= 1;
);

bufsize = bufpos; bufpos = 0;			// buffer size is 44.1khz/30fps = 1470
n = 0; loop (numStrings,
  gfx_r = Colors [3 * n]     + colorfulness;
  gfx_g = Colors [3 * n + 1] + colorfulness;
  gfx_b = Colors [3 * n + 2] + colorfulness;
  p = 0; loop (bufsize,
    gfx_a = brightness * Wave [p];
    gfx_x = n * colwid; gfx_y = gfx_h * Phase [n];
    gfx_lineto (gfx_x + colwid - 5, gfx_y, 0);
    Phase [n] += Frequency [n];
    Phase [n] > 1 ? Phase [n] -= 1;
    p += 1;
  );
  n += 1;
);